package org.bulbagarden.model;

import android.support.annotation.NonNull;

public interface EnumStr {
    @NonNull String str();
}
