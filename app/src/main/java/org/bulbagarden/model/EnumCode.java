package org.bulbagarden.model;

public interface EnumCode {
    int code();
}
