package org.bulbagarden.dataclient.page;

import android.support.annotation.NonNull;

import org.bulbagarden.page.Page;
import org.bulbagarden.page.Section;

import java.util.List;

/**
 * Gson POJI for loading remaining page content.
 */
public interface PageRemaining {
    void mergeInto(Page page);
    @NonNull List<Section> sections();
}
