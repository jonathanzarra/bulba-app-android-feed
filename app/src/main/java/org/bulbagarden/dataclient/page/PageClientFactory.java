package org.bulbagarden.dataclient.page;

import android.support.annotation.NonNull;

import org.bulbagarden.dataclient.WikiSite;
import org.bulbagarden.dataclient.mwapi.page.MwPageClient;
import org.bulbagarden.dataclient.mwapi.page.MwPageService;
import org.bulbagarden.dataclient.restbase.page.RbPageClient;
import org.bulbagarden.dataclient.restbase.page.RbPageService;
import org.bulbagarden.dataclient.retrofit.MwCachedService;
import org.bulbagarden.dataclient.retrofit.RbCachedService;
import org.bulbagarden.dataclient.retrofit.WikiCachedService;
import org.bulbagarden.page.Namespace;
import org.bulbagarden.settings.RbSwitch;

/**
 * This redirection exists because we want to be able to switch between the traditional
 * MediaWiki PHP API and the new Nodejs Mobile Content Service hosted in the RESTBase
 * infrastructure.
 */
public final class PageClientFactory {
    @NonNull private static final WikiCachedService<RbPageService> RESTBASE_CACHE
            = new RbCachedService<>(RbPageService.class);
    @NonNull private static final WikiCachedService<MwPageService> MEDIAWIKI_CACHE
            = new MwCachedService<>(MwPageService.class);

    // TODO: remove the namespace check if and when Parsoid's handling of File pages is updated
    // T135242
    public static PageClient create(@NonNull WikiSite wiki, @NonNull Namespace namespace) {
        if (RbSwitch.INSTANCE.isRestBaseEnabled(wiki) && !namespace.file()) {
            return new RbPageClient(RESTBASE_CACHE.service(wiki));
        }
        return new MwPageClient(MEDIAWIKI_CACHE.service(wiki));
    }

    private PageClientFactory() { }
}
