package org.bulbagarden.readinglist.page.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import org.bulbagarden.database.async.AsyncTable;
import org.bulbagarden.database.column.Column;
import org.bulbagarden.database.contract.ReadingListPageContract;
import org.bulbagarden.readinglist.page.ReadingListPageRow;
import org.bulbagarden.readinglist.page.database.disk.DiskRow;
import org.bulbagarden.readinglist.page.database.disk.DiskStatus;
import org.bulbagarden.readinglist.page.database.disk.ReadingListPageDiskRow;

public class ReadingListPageDiskTable
        extends AsyncTable<DiskStatus, ReadingListPageRow, DiskRow<ReadingListPageRow>> {
    private static final int DATABASE_VERSION = 12;

    public ReadingListPageDiskTable() {
        super(ReadingListPageContract.TABLE_DISK, ReadingListPageContract.Disk.URI,
                ReadingListPageContract.DISK_COLS);
    }

    @Override public ReadingListPageDiskRow fromCursor(@NonNull Cursor cursor) {
        DiskRow<ReadingListPageRow> diskRow = ReadingListPageContract.DISK_COLS.val(cursor);
        return new ReadingListPageDiskRow(diskRow, null);
    }

    @NonNull @Override public Column<?>[] getColumnsAdded(int version) {
        switch (version) {
            case DATABASE_VERSION:
                Column<?>[] diskCols = super.getColumnsAdded(version);
                Column<?>[] cols = new Column<?>[diskCols.length + 1];
                System.arraycopy(diskCols, 0, cols, 0, diskCols.length);
                cols[diskCols.length] = ReadingListPageContract.DiskCol.FILENAME;
                return cols;
            default:
                return super.getColumnsAdded(version);
        }
    }

    @Override protected ContentValues toContentValues(@NonNull DiskRow<ReadingListPageRow> row) {
        return ReadingListPageContract.DISK_COLS.toContentValues(row);
    }

    @Override protected int getDBVersionIntroducedAt() {
        return DATABASE_VERSION;
    }
}
