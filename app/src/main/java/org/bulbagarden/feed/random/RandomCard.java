package org.bulbagarden.feed.random;

import android.support.annotation.NonNull;

import org.bulbagarden.dataclient.WikiSite;
import org.bulbagarden.feed.model.Card;
import org.bulbagarden.feed.model.CardType;

public class RandomCard extends Card {
    @NonNull private WikiSite wiki;

    public RandomCard(@NonNull WikiSite wiki) {
        this.wiki = wiki;
    }

    @NonNull @Override public CardType type() {
        return CardType.RANDOM;
    }

    public WikiSite wikiSite() {
        return wiki;
    }
}
