package org.bulbagarden.feed.mainpage;

import org.bulbagarden.dataclient.WikiSite;
import org.bulbagarden.feed.dataclient.DummyClient;
import org.bulbagarden.feed.model.Card;

public class MainPageClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new MainPageCard();
    }
}
