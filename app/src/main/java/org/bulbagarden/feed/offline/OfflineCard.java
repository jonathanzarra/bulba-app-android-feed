package org.bulbagarden.feed.offline;

import android.support.annotation.NonNull;

import org.bulbagarden.feed.model.Card;
import org.bulbagarden.feed.model.CardType;

public class OfflineCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.OFFLINE;
    }
}
