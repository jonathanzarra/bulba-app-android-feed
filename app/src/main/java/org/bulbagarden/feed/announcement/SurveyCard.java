package org.bulbagarden.feed.announcement;

import android.support.annotation.NonNull;

import org.bulbagarden.feed.model.CardType;

public class SurveyCard extends AnnouncementCard {

    public SurveyCard(@NonNull Announcement announcement) {
        super(announcement);
    }

    @NonNull @Override public CardType type() {
        return CardType.SURVEY;
    }
}
