package org.bulbagarden.feed.searchbar;

import org.bulbagarden.dataclient.WikiSite;
import org.bulbagarden.feed.dataclient.DummyClient;
import org.bulbagarden.feed.model.Card;

public class SearchClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new SearchCard();
    }
}
