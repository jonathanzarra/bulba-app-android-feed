package org.bulbagarden.feed.searchbar;

import android.support.annotation.NonNull;

import org.bulbagarden.feed.model.Card;
import org.bulbagarden.feed.model.CardType;

public class SearchCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.SEARCH_BAR;
    }
}
