package org.bulbagarden.descriptions;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;

import org.bulbagarden.R;
import org.bulbagarden.model.EnumCode;
import org.bulbagarden.model.EnumCodeMap;

enum DescriptionEditTutorialPage implements EnumCode {
    PAGE_ONE(R.layout.inflate_description_edit_tutorial_page_one),
    PAGE_TWO(R.layout.inflate_description_edit_tutorial_page_two);

    private static EnumCodeMap<DescriptionEditTutorialPage> MAP
            = new EnumCodeMap<>(DescriptionEditTutorialPage.class);

    @LayoutRes private final int layout;

    int getLayout() {
        return layout;
    }

    @NonNull public static DescriptionEditTutorialPage of(int code) {
        return MAP.get(code);
    }

    public boolean isLast() {
        return ordinal() == size() - 1;
    }

    public static int size() {
        return MAP.size();
    }

    @Override public int code() {
        return ordinal();
    }

    DescriptionEditTutorialPage(@LayoutRes int layout) {
        this.layout = layout;
    }
}
