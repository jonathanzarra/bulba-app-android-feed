package org.bulbagarden.page.bottomcontent;

import org.bulbagarden.page.PageTitle;

public interface BottomContentInterface {

    void hide();
    void beginLayout();
    PageTitle getTitle();
    void setTitle(PageTitle newTitle);

}
