package org.bulbagarden.edit.richtext;

import android.os.Parcelable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.bulbagarden.richtext.URLSpanNoUnderline;
import org.bulbagarden.test.TestParcelUtil;
import org.bulbagarden.test.TestRunner;

@RunWith(TestRunner.class) public class URLSpanNoUnderlineTest {
    @Test public void testCtorParcel() throws Throwable {
        Parcelable subject = new URLSpanNoUnderline("url");
        TestParcelUtil.test(subject);
    }
}
