package org.bulbagarden.edit.richtext;

import android.os.Parcelable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.bulbagarden.richtext.URLSpanBoldNoUnderline;
import org.bulbagarden.test.TestParcelUtil;
import org.bulbagarden.test.TestRunner;

@RunWith(TestRunner.class) public class URLSpanBoldNoUnderlineTest {
    @Test public void testCtorParcel() throws Throwable {
        Parcelable subject = new URLSpanBoldNoUnderline("url");
        TestParcelUtil.test(subject);
    }
}
