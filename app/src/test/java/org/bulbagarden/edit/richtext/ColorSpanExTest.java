package org.bulbagarden.edit.richtext;

import android.os.Parcelable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.bulbagarden.test.TestParcelUtil;
import org.bulbagarden.test.TestRunner;

@RunWith(TestRunner.class) public class ColorSpanExTest {
    @Test public void testCtorParcel() throws Throwable {
        SyntaxRule rule = new SyntaxRule("startSymbol", "endSymbol", SyntaxRuleStyle.BOLD);
        Parcelable subject = new ColorSpanEx(1, 2, 10, rule);
        TestParcelUtil.test(subject);
    }
}
