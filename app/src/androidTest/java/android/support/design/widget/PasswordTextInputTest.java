package android.support.design.widget;


import org.junit.Before;
import org.junit.experimental.theories.Theory;
import org.bulbagarden.test.theories.TestedOnBool;
import org.bulbagarden.test.view.FontScale;
import org.bulbagarden.test.view.LayoutDirection;
import org.bulbagarden.test.view.ViewTest;
import org.bulbagarden.theme.Theme;

import static android.support.design.widget.PasswordTextInput.OnShowPasswordClickListener;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.bulbagarden.test.TestUtil.runOnMainSync;

public class PasswordTextInputTest extends ViewTest {
    private PasswordTextInput subject;

    @Before public void setUp() {
        setUp(WIDTH_DP_S, LayoutDirection.LOCALE, FontScale.DEFAULT, Theme.LIGHT);
        subject = new PasswordTextInput(ctx());
        subject.setPasswordVisibilityToggleEnabled(true);
    }

    @Theory public void testIsPasswordVisible(@TestedOnBool boolean visible) {
        if (visible) {
            toggleVisibility();
        }
        assertThat(subject.isPasswordVisible(), is(visible));
    }

    @Theory public void testSetOnShowPasswordListener(@TestedOnBool boolean nul,
                                                      @TestedOnBool boolean visible) {
        OnShowPasswordClickListener listener = nul ? null : mock(OnShowPasswordClickListener.class);
        if (visible) {
            toggleVisibility();
        }
        subject.setOnShowPasswordListener(listener);
        toggleVisibility();
        if (listener != null) {
            verify(listener).onShowPasswordClick(eq(!visible));
        }
    }

    private void toggleVisibility() {
        runOnMainSync(new Runnable() {
            @Override
            public void run() {
                subject.passwordVisibilityToggleRequested();
            }
        });
    }
}
